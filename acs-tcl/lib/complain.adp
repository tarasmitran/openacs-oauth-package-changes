<master>
  <property name="title">#acs-tcl.lt_Problem_with_your_inp#</property>
<%
set this_host [ns_conn location]
regsub {http:} $this_host {https:} this_host
set oh  [ns_conn outputheaders]
ns_set update $oh Cache-control no-store
ns_set update $oh X-XSS-Protection 0
ns_set update $oh X-FRAME-OPTIONS DENY
ns_set update $oh X-FRAME-OPTIONS SAMEORIGIN
ns_set update $oh X-CONTENT-TYPE-OPTIONS NOSNIFF
ns_set update $oh X-DOWNLOAD-OPTIONS NOOPEN
ns_set update $oh X-CONTENT-SECURITY-POLICY "allow $this_host; img-src 'self' data:; options inline-script eval-script;"
ns_set update $oh ACCESS-CONTROL-ALLOW-ORIGIN "$this_host"

%>


<p>
  #acs-tcl.We_had#
  <if @complaints:rowcount@ gt 1>#acs-tcl.some_problems#</if>
  <else>#acs-tcl.a_problem#</else>
  #acs-tcl.with_your_input#
</p>

<ul>
  <multiple name="complaints">
    <li>@complaints.text;noquote@</li>
  </multiple>
</ul>

<p>
  #acs-tcl.lt_Please_back_up_using_# <if @complaints:rowcount@ gt 1>#acs-tcl.errors#</if><else>#acs-tcl.error#</else>#acs-tcl.lt__and_resubmit_your_en#
</p>

<p>
  #acs-tcl.Thank_you#
</p>

