ad_page_contract {
    Prompt the user for email and password.
    @cvs-id $Id: index.tcl,v 1.3 2007/10/14 23:40:22 cvs_guest Exp $
} {
    { email "" }
    return_url:optional
} -validate {
  return_url_ck -requires return_url:notnull {
    if {[string length $return_url] < 1 ||
        [string is space $return_url] ||
        [string first " " $return_url]  == 0} {
            
      ns_set put [ns_conn outputheaders] "X-Is-Login-Form" "true"
      ad_complain "Invalid Return. The return url that you entered is invalid"
    }
    set test_return_url [string map {/ a ? a - a _ a % a # a & a = a . a : a} $return_url]
    #ns_log notice "test_return_url=$test_return_url"
    if {![string is alnum $test_return_url] } {
      ns_set put [ns_conn outputheaders] "X-Is-Login-Form" "true"
      ad_complain "Invalid Return. The return url that you entered is not valid"
    }
  }

} -properties {
    system_name:onevalue
    export_vars:onevalue
    email:onevalue
}

ns_set put [ns_conn outputheaders] "X-Is-Login-Form" "true"
#ns_log notice "REGISTER/INDEX 
#               enter"
if {[info exists return_url] && [string length $return_url] > 0} {
  # send them to the subsite that they came from to login in
  set url_list [string map {/ \ } $return_url]
  set subsite [lindex $url_list 0]
  if {[string length $subsite] > 0} {
    set is_valid_account 0

    db_foreach subsites {select url from merit_accounts} {
      if {$subsite == $url} {
        set is_valid_account 1
        break
      }
    }
   
    if {$is_valid_account == 1} {
      # send them whence they came
      # log the user out for extra security 
      # - teach them not to come to this page
      if { [catch {set cookie_list [ad_get_signed_cookie_with_expr "ad_session_id"]} errmsg]} {
        ad_returnredirect "$subsite/login"
      } else {
         set new_user_id 0
         if { ![ad_secure_conn_p] } {
            catch {
                set new_user_id [ad_get_signed_cookie "ad_user_login"]
            }
         } else {
            catch {
                set new_user_id [lindex [split [ad_get_signed_cookie "ad_user_l gin_secure"] {,}] 0]
            }
        }
        set from_login [expr [string match "/$subsite" $return_url] ||\
                             [string match "/$subsite/" $return_url] ||\
                             [string match "/$subsite/?" $return_url] ||\
                             [string match "/$subsite/login" $return_url] ||\
                             [string match "/$subsite/login/" $return_url] ||\
                             [string match "/$subsite/login/?" $return_url]]
        if {$new_user_id == 0 && !$from_login} {
          ad_returnredirect "/$subsite/login?[export_vars -sign {expired_login 1}]"
        } else {
          ad_returnredirect "/$subsite/login"
        }
      }
      ad_user_logout
      return
    }
  }
}

if {![info exists return_url]} {
    set return_url [ad_pvt_home]
}
set system_name [ad_system_name]

# One common problem with login is that people can hit the back button
# after a user logs out and relogin by using the cached password in
# the browser. We generate a unique hashed timestamp so that users
# cannot use the back button.

set in_time [ns_time]
set in_token_id [sec_get_random_cached_token_id]
set token [sec_get_token $in_token_id]
set in_hash [ns_sha1 "$in_time$in_token_id$token"]

set exp_vars [export_vars -form -sign {return_url in_time in_token_id in_hash}]

# only admins can add users so for extra security logout anyone whe ends up here
ad_user_logout
db_release_unused_handles

ns_set put [ns_conn outputheaders] "X-Is-Login-Form" "true"

ad_return_template
