ad_page_contract {
    Front end to the password-remind feature 
} {
  subsite_url:verify,notnull
}


set _nonce_timestamp [clock clicks]
set _nonce_string "[ad_generate_random_string 12]"
set _nonce "${_nonce_string}-${_nonce_timestamp}"
ad_set_client_property -persistent "false" -session_id [ad_conn session_id]  [ad_conn package_id] mp-email-password-${_nonce_timestamp} $_nonce


set exp_vars [export_vars -sign -form {subsite_url _nonce}]
ns_log notice "MP-EMAIL-PASSWORD
               subsite_url=$subsite_url"

ad_return_template
