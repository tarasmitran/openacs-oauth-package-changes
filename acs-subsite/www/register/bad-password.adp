<master>
<property name=title>Invalid username/password</property>

<hr>

<p class="warning">Log-in information incorrect or invalid. </p>

<p class="warning">If you are a registered subscriber, you may have entered your e-mail address 
or password incorrectly. Go back and try again.</p>

<p class="warning">If you are not registered, please contact your local MarketPay Administrator 
to gain access to this site.</p>

<p class="warning">If you need help, <a href="mailto:mark@marketpay.com">please contact us.</a></p>
