<master>
<property name=title>Email Password</property>

<b>Reset and Email New Password</b>
<br>
<br>
If you are a valid registered user and you enter your email address correctly 
a new password will be emailed to you shortly so that you can log in.
<br>
<br>
<form method="post" action="mp-email-password-2">
  @exp_vars;noquote@
  Please enter your email address: <input type="text" size=30 name="email">
  <br>
  <br>
  <input type=submit value="Reset and Email New Password">
</form>

