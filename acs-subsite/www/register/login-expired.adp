<master>
<property name=title>Login Expired</property>


<p>

<font size=-1>
<table cellpadding=5 cellspacing=1 border=1>
  <tr>
    <td bgcolor="#ffffcc"><b></b>To prevent unauthorized access, the system has logged you out of MarketPay due to inactivity. 
Please 
<if @return_url@ nil>
login again.
</if><else>
<a href="../@return_url@/login.tcl">login again</a>.
</else>
    </td>
  </tr>
</table>
</font>


