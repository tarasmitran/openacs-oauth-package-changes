<master>
<property name=title>Request Received</property>
<br>
<br>
If you are a registered user and if you have entered your e-mail address 
accurately, a new password has been sent to that e-mail address. 
If not, MarketPay staff have been notified.
<br>
<br>
<a href="/@subsite_url@/login">Return to Login Page</a> 

