ad_page_contract {

    Registration form for a new user.  The password property should be set using 
    <code>ad_set_client_property register</code>.

    @cvs-id  $Id: user-new-marketpay.tcl,v 1.2 2004/09/29 20:43:35 nsadmin Exp $

} {
} -validate {
} -properties {
}

set system_name [ad_system_name]
 set subsite_path [ad_conn package_url]
 set subsite [string trim $subsite_path "/"]

set subsite_package_id [ad_conn package_id]
set subsite_node_id [ad_conn node_id]
set exp_vars [export_vars -sign {{subsite_url $subsite}}]

db_0or1row get_subsite_url "select name as subsite_url
                              from site_nodes
                              where object_id=:subsite_package_id"

 ns_log Notice "user-new-marketpay $subsite_path $subsite url $subsite_url"

 set client_mgr_email ""
 set client_mgr_str ""
 if {[string length $subsite_url]} {
   db_0or1row get_cm "select client_mgr_email 
	from merit_accounts
	where url = :subsite_url"
 }

  if {[string length $client_mgr_email] && ![string equal $client_mgr_email "chighfield@marketpay.com"]} {
	set client_mgr_str "$client_mgr_email"
   }




ad_return_template



