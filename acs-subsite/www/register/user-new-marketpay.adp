<master>
<property name=title>Invalid Login</property>

<br><br>

<p class="warning">Log-in information incorrect or invalid. </p>
                                                                                
<p class="warning">If you are a registered user, you may have entered your e-mail address
or password incorrectly. Click the Back button to return to the login page and re-enter your login credentials.</p>
                                                                                
<p class="warning">If you are not registered, please contact your local MarketPay Administrator
to gain access to this site.</p>

<% set site_exp_var [export_vars -sign {subsite_url}] %>
<p class="warning"><a href="/register/mp-email-password?@exp_vars@">Click here</a> if you have forgotten your password.</p>


<if @client_mgr_str@ ne "">                                                                                
<p class="warning">If you need help, <a href="mailto:helpdesk@marketpay.com">please contact us.</a></p>
</if>
<else>
<p class="warning">If you need help, <a href="mailto:helpdesk@marketpay.com">please contact us.</a></p>
</else>

