ad_page_contract {
    Prompt the user for email and password.
    @cvs-id $Id: login-expired.tcl,v 1.3 2007/10/14 23:40:22 cvs_guest Exp $
} {
    return_url:optional
} -validate {
  return_url_ck -requires return_url:notnull {
    if {[string length $return_url] < 1 ||
        [string is space $return_url] ||
        [string first " " $return_url]  == 0} {
      ad_complain "Invalid Return. The return url that you entered is invalid"
    }
    set test_return_url [string map {/ a ? a - a _ a} $return_url]
    if {![string is alnum $test_return_url] } {
      ad_complain "Invalid Return. The return url that you entered is invalid"
    }
  }

} -properties {
    system_name:onevalue
    export_vars:onevalue
    email:onevalue
    old_login_process:onevalue
    allow_persistent_login_p:onevalue
    persistent_login_p:onevalue
}

ad_return_template
