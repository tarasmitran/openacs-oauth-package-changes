ad_page_contract {

    Accepts an email from the user and attempts to log the user in.

    @author Multiple
    @cvs-id $Id: user-login.tcl,v 1.11 2005/02/16 18:23:47 nsadmin Exp $
} {
    {email ""}
    {return_url [ad_pvt_home]}
    {password ""}
    in_time:notnull,integer,verify,trim
    in_token_id:notnull,integer,verify,trim
    in_hash:notnull,verify,trim
    {SimpleSAMLAuthToken:verify,trim ""}
} -validate {
  return_url_ck -requires return_url:notnull {
    if {[string length $return_url] < 1 ||
        [string is space $return_url] ||
        [string first " " $return_url]  == 0} {
      ad_complain "Invalid Return. The return url that you entered is invalid"
    }
    set test_return_url [string map {/ a ? a - a _ a} $return_url]
    if {![string is alnum $test_return_url] } {
      ad_complain "Invalid Return. The return url that you entered is invalid"
    }
  }
  token_ck {
    if {![info exists in_token_id] ||
        [string length $in_token_id] < 1 ||
        [string length $in_token_id] > 4 ||
        [string is space $in_token_id] ||
        [string first " " $in_token_id]  == 0 ||
        ![string is integer $in_token_id]} {
      ns_log error "URL Surgery - in_token_id register/user-login"
      ad_complain "Invalid Token ID. Probably caused by url surgery. This incident has been recorded"
    }
  }
  time_ck {
    if {![info exists in_time] ||
        [string length $in_time] < 1 ||
        [string is space $in_time] ||
        [string first " " $in_time]  == 0 ||
        ![string is integer $in_time]} {
      ns_log error "URL Surgery - in_time register/user-login"
      ad_complain "Invalid Time. Probably caused by url surgery. This incident has been recorded"
    }
  }
  hash_ck {
    if {![info exists in_hash] ||
        [string length $in_hash] < 1 ||
        [string is space $in_hash] ||
        [string first " " $in_hash]  == 0 ||
        ![string is alnum $in_hash]} {
      ns_log error "URL Surgery - in_hash register/user-login"
      ad_complain "Invalid Hash. Probably caused by url surgery. This incident has been recorded"
    }
  }
}


# don't allow persistent logins no matter what the site parameters say
set persistent_cookie_p 0
# Security check to prevent the back button exploit.
set token [sec_get_token $in_token_id]
set computed_hash [ns_sha1 "$in_time$in_token_id$token"]
set ip_address [ad_conn peeraddr]

#####################
###### SSO ##########
#####################

set logout_url ""
if { [string length $SimpleSAMLAuthToken] } {

  set sso_server [ns_config ns/parameters SSOServer 0]
  set sso_status [catch { exec curl -s https://${sso_server}:8080/mp/getSAMLInfo.php?SimpleSAMLAuthToken=${SimpleSAMLAuthToken}}  sso_string ]
  
  ns_log notice "sso_string $sso_string"
  ns_log notice "sso_string_end"
  # if {[catch {set key_val [::json::json2dict $key_val]} errmsg]} {}
  set login_data [::json::json2dict $sso_string]
  
  
  #first level error, if the sso server returns any type of error, then jump to error page
  if {[catch { set login_error [dict get $login_data "error"] }]} {
  set login_error ""
  }
  if {[string length  $login_error] } {
     ns_log notice "sso FAILURE "
      login_failure $ip_address
      #ad_returnredirect "user-new-marketpay"
      ad_return_error "Problem with login" "We have single signed you into your subsite.  However, you are not registered as a user in MarketPay, so you cannot access the system.  Please contact your MarketPay system administrator and request to be added as a MarketPay user.  Error Code # 001."
      ad_script_abort
  }
  #basically if we get an email from the sso server, then we have a mostly valid login.  Just check permissions
  if {[catch { set email [dict get $login_data "emailaddress"] }]} {
    set email ""
  }
  if {[catch { set logout_url [string trim [dict get $login_data "logoutURL"]] }]} { 
    set logout_url  "" 
  }  
#else {
  
     ##make sure we know the correct logout url will be same sitewide
     #set subsite_package_id [ad_conn package_id]
     #set subsite_node_id [ad_conn node_id]
 # 
 #    db_0or1row get_subsite_url "select name as subsite_url
 #                                  from site_nodes 
 #                                  where object_id=:subsite_package_id"
 # 
 #    set sso ""
 #    db_0or1row get_subsite_url "select sso  
 #                                  from merit_accounts 
 #                                  where url=:subsite_url"
 #    if { ! [string equal $sso $logout_url ] } {
 #      if { [string length $logout_url] > 0 } {
 #        ns_log notice "updating sso for $subsite_url with sso= $logout_url"
 #        db_dml update_sso "update merit_accounts set sso = :logout_url where url=:subsite_url"
 #      }
 #    }
 # }
}



#ad_returnredirect "$return_url"
#ad_script_abort

#####################
###### end SSO ######
#####################


#set password ""
#ns_log Notice "USER-LOGIN
#               email=$email
#               return_url=$return_url
#               password=$password
#               persistent_cookie_p=$persistent_cookie_p
#               in_token_id=$in_token_id
#               token=$token
#               in_time=$in_time
#               in_hash=$in_hash
#               computed_hash=$computed_hash
#               ip_address=$ip_address"
set is_blocked [db_0or1row check_blocked "
                    select blocked_ip
                      from merit_crack_block
                      where blocked_ip=:ip_address "]

if {$is_blocked} {
#first login code, put it back in.
  ns_log error "login.tcl SECURITY VIOLATION A
                  Attempt to login from blocked IP Address $ip_address"
  ad_user_logout
  ad_returnredirect "security-violation"
} 

# Obtain the user ID corresponding to the provided email address.
set email [string tolower [string trim $email]]
ns_log Notice "**SECURITY** packages/acs-subsite/www/register/user-login $email $ip_address"


set this_package_key [ad_conn package_key]

if {$this_package_key == "acs-subsite"} {
  set subsite_id [ad_conn package_id]
  set subsite_path [ad_conn package_url]
} else {
  set subsite_id [site_node_closest_ancestor_package "acs-subsite"]
  set subsite_path [site_node_closest_ancestor_package_url -package_key "acs-subsite"]
}
set subsite [string trim $subsite_path "/"]
set user_agent ""
set user_agent [ns_set iget [ns_conn headers] User-Agent]

# (dave) User has attempted a login, record it 
  db_dml insert_login_log "insert into merit_login_log (EMAIL,LOGIN_URL,LOGIN_DATE,IP_ADDRESS,SUCCESSFUL_P, user_agent)
  values (:email, :subsite, current_timestamp, :ip_address, 'f',:user_agent)"

ns_log notice "per account_lockouts  [ login_pre_check $subsite $email ] "
# per account locouts
if { ![ login_pre_check $subsite $email ] && ![string length $SimpleSAMLAuthToken] } {
  login_failure $ip_address
  ad_returnredirect "user-new-marketpay"
  ad_script_abort
}


if { [string compare $in_hash $computed_hash] != 0 } {
    # although this technically is not an expired login, we'll
    # just use it anyway.
    ad_returnredirect "login-expired"
    return
} elseif { $in_time < [ns_time] - [ad_parameter -package_id [ad_acs_kernel_id] LoginExpirationTime security 7200] } {
    ad_returnredirect "login-expired"
    return
}

if { ![db_0or1row user_login_user_id_from_email {
    select user_id, member_state, email_verified_p
    from cc_users
    where email = :email}] } {

    # The user is not in the database. Redirect to user-new.tcl so the user can register.
#    ad_returnredirect "user-new?[ad_export_vars { email return_url persistent_cookie_p }]"

    update_user_failure $email incr
    ns_log Notice "packages user-login failed to verify ->$email<- in db. redirect to user-new-marketpay"

    login_failure $ip_address
    if {  [string length $SimpleSAMLAuthToken] } {
      ad_return_error "Problem with login" "We have single signed you into your subsite.  However, you are not registered as a user in MarketPay, so you cannot access the system.  <br/><br/>Please contact your MarketPay system administrator and request to be added as a MarketPay user.  Error Code # 001."
      ad_script_abort
    } else {
      ad_returnredirect "user-new-marketpay"
    } 
    return
}

### part of sso login - request from rich, check superadmins are not being SSO from another server rather than internal to Marketpay.  Not sure what this is suppose to accomplish
if { [string length $SimpleSAMLAuthToken] && [group::member_p -user_id $user_id -group_name superadmins] } {
  if {[catch { set sso_source [dict get $login_data "company"] }]} {
    set sso_source ""
  }
  ns_log notice "super_admin [group::member_p -user_id $user_id -group_name superadmins] "
  ns_log notice "sso_source $sso_source"
  if { $sso_source != "MarketPay" } {
      ad_return_error "Problem with login" "We have single signed you into your subsite.  However, you are not registered as a user in MarketPay, so you cannot access the system. <br/> <br/>Please contact your MarketPay system administrator and request to be added as a MarketPay user.  Error Code # 001."
      ad_script_abort
  }
}
## end of hard coded check for rich

db_release_unused_handles

switch $member_state {
    "approved" {
	if { $email_verified_p == "f" } {
	    ad_returnredirect "awaiting-email-verification?user_id=$user_id"
	    return
	}
        #sso change for saml
	if { [ad_check_password $user_id $password] || ( [string length $SimpleSAMLAuthToken] && [string length $email] )} {
            ivc_oauthPwGrant -username $email -pw $password
            update_user_failure $email 0
	    # The user has provided a correct, non-empty password. Log
	    # him/her in and redirect to return_url.
            set this_package_key [ad_conn package_key]
                                                                                
            if {$this_package_key == "acs-subsite"} {
              set subsite_id [ad_conn package_id]
              set subsite_path [ad_conn package_url]
            } else {
              set subsite_id [site_node_closest_ancestor_package "acs-subsite"]
              set subsite_path [site_node_closest_ancestor_package_url -package_key "acs-subsite"]
            }
            set subsite [string trim $subsite_path "/"]
            set party_name "$subsite Parties"
            db_1row group_info {
              select group_id
                from application_groups
                where package_id = :subsite_id
            }
            ns_log Notice "group_id is $group_id based on subsite_id $subsite_id"
            set is_group_member [db_0or1row get_membership_rel "\
                                   select rel_id \
                                     from acs_rels \
                                     where rel_type='membership_rel' and \
                                           object_id_one=:group_id and \
                                           object_id_two=:user_id"]
            if {$is_group_member == 0} {
              #ns_log notice "USER-LOGIN.TCL
              #               user $user_id 
              #               is not a  member of the subsite"
              # see if they are a RO or RU group member
              set  ro_party_name "merit-$subsite RO Parties"
              set  ru_party_name "merit-$subsite RU Parties"
              set has_ro_group [db_0or1row group_info " \
                select group_id as ro_group_id  \
                  from groups  \
                  where group_name = :ro_party_name"]
              set has_ru_group [db_0or1row group_info " \
                select group_id as ru_group_id  \
                  from groups  \
                  where group_name = :ru_party_name"]
              #ns_log notice "USER-LOGIN.TCL
              #               has_ro_group=$has_ro_group
              #               has_ru_group=$has_ru_group"
              if {$has_ro_group == 1} {
                # ro group exists - make sure it is an application group
                set is_application_group [db_0or1row app_group_info " \
                  select package_id as app_package_id \
                    from application_groups \
                    where group_id=:ro_group_id"]
                if {$is_application_group == 1} {
                  # make sure that it is the merit package 
                  # mounted under the current subsite
                  set is_merit_package [db_0or1row pkg_info " \
                    select instance_name as subsite_pkg_name \
                      from apm_packages \
                      where package_id=:app_package_id"]
                  if {$subsite_pkg_name == "merit-$subsite"} {
                    # see if ther are a member of the group
                    set is_group_member [db_0or1row get_membership_rel "\
                                   select rel_id \
                                     from acs_rels \
                                     where rel_type='membership_rel' and \
                                           object_id_one=:ro_group_id and \
                                           object_id_two=:user_id"]
                  }
                }
              } 
              if {$is_group_member == 0 && $has_ru_group == 1} {
                #ns_log notice "USER-LOGIN.TCL
                #               Testing to see if user $user_id 
                #               is a member of the ru group"
                # make sure that it is the merit package 
                # mounted under the current subsite
                set subsite_pkg_name "merit-$subsite"
                set is_merit_package [db_0or1row pkg_info " \
                  select package_id as app_package_id \
                    from apm_packages \
                    where instance_name =:subsite_pkg_name"]
                if {$is_merit_package} {
                  #ns_log notice "USER-LOGIN.TCL
                  #               package is a merit package"
                  # see if ther are a member of the group
                  set is_group_member [db_0or1row get_membership_rel "\
                                 select rel_id \
                                   from acs_rels \
                                   where rel_type='membership_rel' and \
                                         object_id_one=:ru_group_id and \
                                         object_id_two=:user_id"]
                }
                #ns_log notice "USER-LOGIN.TCL
                #               is_group_member=$is_group_member"
                
              }
            }
            set has_admin_p [ad_permission_p -user_id $user_id $group_id "admin"]
ns_log notice "USER_LOGIN
               user_id $user_id
               subsite = $subsite
               subsite_id = $subsite_id
               is_group_member = $is_group_member
               has_admin_p = $has_admin_p"



            if {$is_group_member == 1 || $has_admin_p == 1} { 
	      ad_user_login -forever=$persistent_cookie_p $user_id

              #(dave) User has logged in. Update row to successful.
              db_dml update_login_log "update merit_login_log 
                                         set SUCCESSFUL_P = 't' 
                                         where EMAIL = :email and 
                                         to_char(login_date,'YYYY-MM-DD HH24:mi:ss') =
                                         to_char(current_timestamp,'YYYY-MM-DD HH24:mi:ss')"
	    
            } else {
#first login code, put it back in.
              ns_log error "USER_LOGIN SECURITY VIOLATION B
              is_group_member $is_group_member has_admin_p $has_admin_p for group $group_id             
                             user $user_id attempt to login to $subsite"
              login_failure $ip_address
  	      ad_returnredirect "security-violation"
              return
            }
  
  
            db_0or1row get_user_info  "select sso_logout
                                          from users 
                                          where user_id=:user_id"
         
            if { ! [string equal $sso_logout $logout_url ] } {
              if { [string length $logout_url] > 0 } {
                ns_log notice "updating sso for $user_id with sso_= $logout_url"
                db_dml update_sso "update users set sso_logout = :logout_url where user_id=:user_id"
              }
            }
  	    ad_returnredirect $return_url
	    return
	} {
           update_user_failure $email incr
           ns_log Notice "ad_check_password failed for $password "
          }
    }
    "banned" { 
	ad_returnredirect "banned-user?user_id=$user_id" 
	return
    }
    "deleted" {  
	ad_returnredirect "deleted-user?user_id=$user_id" 
	return
    }
    "rejected" {
	ad_returnredirect "awaiting-approval?user_id=$user_id"
	return
    }
    "needs approval" {
	ad_returnredirect "awaiting-approval?user_id=$user_id"
	return
    }
    default {
        update_user_failure $email incr
	ns_log Error "Problem with registration state machine on user-login.tcl"
        login_failure $ip_address
	ad_return_error "Problem with login" "There was a problem authenticating the account: $user_id."
	return
    }
}

# The user is in the database, but has provided an incorrect password.
ns_log Notice "USER_LOGIN (Possible) SECURITY VIOLATION  -- valid email but bad password
              user $user_id attempt to login to [ad_conn package_url]"
login_failure $ip_address

ad_returnredirect "user-new-marketpay"
