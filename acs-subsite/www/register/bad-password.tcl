# /www/register/bad-password.tcl

ad_page_contract {
    Informs the user that they have typed in a bad password.
    @cvs-id $Id: bad-password.tcl,v 1.1 2004/10/22 15:22:02 nsadmin Exp $
} {
    {return_url ""}
} -validate {
  return_url_ck -requires return_url:notnull {
    if {[string length $return_url] < 1 ||
        [string is space $return_url] ||
        [string first " " $return_url]  == 0} {
      ad_complain "Invalid Return. The return url that you entered is invalid"
    }
    set test_return_url [string map {/ a ? a - a _ a} $return_url]
    if {![string is alnum $test_return_url] } {
      ad_complain "Invalid Return. The return url that you entered is invalid"
    }
  }

} -properties {
    system_name:onevalue
    email_forgotten_password_p:onevalue
    user_id:onevalue
}

#set email_forgotten_password_p [ad_parameter EmailForgottenPasswordP security 1]

set system_name [ad_system_name]

ad_return_template
