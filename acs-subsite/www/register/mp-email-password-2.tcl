ad_page_contract {
    Front end to the password-remind feature 
} {
    subsite_url:verify,notnull
    email:notnull
   _nonce:verify
} -properties {
} -validate {
  nonce_check {
    set _nonce_timestamp [lindex [split $_nonce -] 1]

    ns_log notice "nonce $_nonce  _nonce_timestamp $_nonce_timestamp "
    set cached_nonce  [ad_get_client_property -cache_only "t" -default "0"  -session_id [ad_conn session_id]  [ad_conn package_id]  "mp-email-password-${_nonce_timestamp}" ]
    ad_set_client_property -persistent "false" -session_id [ad_conn session_id]  [ad_conn package_id] mp-email-password-${_nonce_timestamp} ""

    if { [string compare $cached_nonce $_nonce] != 0  }  {ad_complain "Error. " }
  }

}


set ip_address [ad_conn peeraddr]
#SET temp take this out until I track down where in the db it comes from so we can set it for all servers
if {0} {
if {![ad_parameter EmailForgottenPasswordP security 1]} {
    ad_return_warning "Feature disabled" "This feature is disabled on this server."
    return
  }
}


if {  [login_pre_check $subsite_url $email] } {

      set client_mgr_email ""
      db_0or1row get_client_mgr_email "select client_mgr_email
        from merit_accounts
        where url=:subsite_url"

      if {[string length $client_mgr_email]} {
       set client_mgr_string ",$client_mgr_email"
       } {
       set client_mgr_string ""
       }


ns_log Notice "in mp-email-password-2 at var/lib/aolserver/merit/packages/acs-subsite/www/register $subsite_url $email"

if {![empty_string_p $email] && ![string is space $email]} {
  set is_registered_p [db_0or1row get_user_id "
                         select party_id as user_id
                           from parties
                           where lower(email)=lower(:email)"]

  if {$is_registered_p} {
#was ivc Parties on wallace -- why is it now just ivc? SET 7/24/08
#    set group_name "$subsite_url Parties"
    set group_name "$subsite_url"
ns_log Notice "in mp-email-password-2 get $group_name from groups"
    db_0or1row get_group_id "select group_id 
                               from groups 
                               where group_name=:group_name"
    set allowed_on_site [db_0or1row get_rel_id "
                            select rel_id 
                              from acs_rels 
                               where object_id_one=:group_id and
                                     object_id_two=:user_id"]


    if {$allowed_on_site} {
      #set password [ad_generate_random_string]
      set password [exec /usr/local/bin/gpw]
      ad_change_password $user_id $password
      db_dml update_pw_expires " update users set tmp_pw_expires = null where user_id=:user_id"
                                                                                
      ns_log Notice "**SECURITY** emailing new password for user $user_id"
      ns_log Notice "**SECURITY** ns_sendmail $email system@marketpay.com [ad_system_name]"
      if [catch { ns_sendmail "$email" system@marketpay.com "Forgotten password on [ad_system_name]" "Your password on $subsite_url has been reset to: Password:  $password\n\nIf you use copy/paste to copy the new password to the MarketPay login page, be sure that you do not copy trailing spaces after the password. If you do, the new password will not work." } errmsg] { 
        ns_log Notice "error sending mail $errmsg"
             ad_return_warning "Error sending mail" \
               "Now we're really in trouble because we got an error trying \
                to send email. Please contact your system administrator: \
               <blockquote> \
               <pre>
               $errmsg
               </pre>
               </blockquote>"
          }
    } else {

  db_dml insert_login_log "insert into merit_login_log (EMAIL,LOGIN_DATE,IP_ADDRESS,SUCCESSFUL_P,login_url,failure_reason)
  values (:email, current_timestamp, :ip_address, 'f',:subsite_url,'attempt to get a new password for valid user not member of subsite')"
      ns_log error "mp-email-password-2.tcl
                    Attempt to get a new password for registered user
                    email=$email 
                    on subsite $subsite_url where not a member"
      if [catch { ns_sendmail "rich@marketpay.com, chighfield@marketpay.com,helpdesk@marketpay.com $client_mgr_string"  system@marketpay.com "Forgotten password on [exec hostname] [ad_system_name] [ns_info server] security violation" "Attempt to get new password with email=$email in subsite=$subsite_url where user is not a member of that site"  } errmsg] {
             ad_return_warning "Error sending mail" \
               "Now we're really in trouble because we got an error trying \
                to send email to ourselves: <blockquote> \
               <pre>
               $errmsg
               </pre>
               </blockquote>"
          }
    }
  } else {
    ns_log error "mp-email-password-2.tcl
                  Attempt to get a new password for unregistered user
                  email=$email 
                  on subsite $subsite_url"
  db_dml insert_login_log "insert into merit_login_log (EMAIL,LOGIN_DATE,IP_ADDRESS,SUCCESSFUL_P,login_url,failure_reason)
  values (:email, current_timestamp, :ip_address, 'f',:subsite_url,'attempt to get a new password for unknown user')"
    if [catch { ns_sendmail "rich@marketpay.com,chighfield@marketpay.com,helpdesk@marketpay.com $client_mgr_string"  system@marketpay.com  "Forgotten password on [ad_system_name] security violation"  "Attempt to get new password for unregistered user with email=$email in subsite=$subsite_url" } errmsg] {
           ad_return_warning "Error sending mail" \
             "Now we're really in trouble because we got an error trying \
              to send email to ourselves: <blockquote> \
             <pre>
             $errmsg
             </pre>
             </blockquote>"
        }
  }
} else {
   ns_log error "mp-email-password-2.tcl
                  Attempt to get a new password for unregistered user
                  using empty email on subsite $subsite_url"

   if [catch {ns_sendmail "rich@marketpay.com, chighfield@marketpay.com,helpdesk@marketpay.com  $client_mgr_string"  system@marketpay.com  "Forgotten password on [ad_system_name] security violation"  "Attempt to spoof system into sending new password with empty email" } errmsg] {
           ad_return_warning "Error sending mail" \
             "Now we're really in trouble because we got an error trying \
              to send email to ourselves: <blockquote> \
             <pre>
             $errmsg
             </pre>
             </blockquote>"
        }
}
}


ad_return_template
