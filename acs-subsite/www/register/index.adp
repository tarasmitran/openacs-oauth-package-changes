<master>
<property name=title>Users Must Log In</property>
<property name="context">{#acs-subsite.Log_In#}</property>
<br>
<br>

<h3 align=center>Welcome to Marketpay Associates</h3>

<hr>

<p><b>Current users:</b> Please enter the site using the main site url supplied by your system administrator.</p>

<p><b>New users:</b> Please contact your system administrator for a valid user name and password and a url for your group</p>

<hr>
<p>
If you keep getting thrown back here, it is probably for of one of the following reasons:</p>
<ul>
<li> You attempted to access this secure site using http instead of https. Try again, using https.</li>
<li> Your session has been inactive and the system has logged you out.</li>
<li> You have attempted to enter the system without first logging in.</li>
<li> You logged in but your organization is filtering access by ip address.</li>
<li> Your  browser does not accept cookies.</li>
</ul>
<p>
We're sorry for the inconvenience but it really is impossible to operate a 
system like this without keeping track of session activity and attempts to access the system.</p>
